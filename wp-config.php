<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nguyenkye' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'f+Z8ivrMQXKTb]uIF!0*Suc?teYb{11b&}%}ZfGwCNYM;6;tFVva}G<p00;Pb}SL' );
define( 'SECURE_AUTH_KEY',  'F_x5}R]Ix&/QDOyt*&A]ql=u%39es{seYEvAcT/XrAAh0C(ee`kL/={`;%-M+fR;' );
define( 'LOGGED_IN_KEY',    '^}6&]vUMJ#l2=q=(.6pH4.tc$BQ*<^{Mlw6rCy_XdCF;ILP&bD32d ?u,Sc0c:fh' );
define( 'NONCE_KEY',        'g;WyD.>1c|^!&=Biw`%=ggC|ls(a-E[>RZU#$>MHw}1<m= 2@hBw[D+IJpPQ%{~D' );
define( 'AUTH_SALT',        'SL_<OUKzewJGQszaiZ KZnXXZ(MU]P u<PJt=b3{Nf92z}UTh)y1{a=7@;cxM.~z' );
define( 'SECURE_AUTH_SALT', 'QfbXq0[J&BMrt[^.iX/=CX#{(v3qOu8$=59!<^t?oajg+ga#|OoIB8inM|Sk)wBm' );
define( 'LOGGED_IN_SALT',   'vM2ANW#4;ECG$&jk{WEs]&7j}X&BXbh=myLz;$Go4H4tg2AFod6*C1YC@>*FZ@s|' );
define( 'NONCE_SALT',       'M9cZ$Gcs2~m0Dj:B.)v3-0vG<O]z/dM#i{/pek,6#R-z jz|#6mB,6oAsAlM.uBq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
